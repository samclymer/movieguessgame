var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var newDate = require("../helpers/dateHelper");

var highScoreSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    score: {
        type: Number,
        required: true
    },
    date: {

        type: String,
        default: newDate
    }
});

// Create the model using the Schema
var Scores = mongoose.model("scores", highScoreSchema);

// Export the model
module.exports = Scores;