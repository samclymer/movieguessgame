var express = require("express");
var mongoose = require("mongoose");
var exphbs = require("express-handlebars");
var routes = require("./routes");
var db = require("./config/keys").mongoURI;

var app = express();

app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");


app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static("Public"));

mongoose.Promise = global.Promise;

mongoose.connect(db, {
    useNewUrlParser: true
})
    .then(function () {
        console.log("MongoDB Connected...")
    })
    .catch(function (err) {
        console.log(err)
    });

app.use(routes);

var PORT = process.env.PORT || 3000;

app.listen(PORT, function () {
    console.log("Listening on port: " + PORT);
});