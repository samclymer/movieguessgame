var express = require("express");
var router = express.Router();
var scoreController = require("../../controllers/highScore");

//router.get("/", scoreController.find);
router.post("/", scoreController.create);
//router.delete("/:id", scoreController.delete);

module.exports = router;