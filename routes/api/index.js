var express = require("express");
var router = express.Router();
var scoreRoute = require("./scoreRoute")

router.use("/scoreRoute", scoreRoute);

module.exports = router;