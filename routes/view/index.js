var express = require("express");
var router = express.Router();
var Scores = require("../../models/Scores");

router.get("/", function (req, res) {
  Scores.find({})
    .sort({ score: -1 })
    .lean()
    .limit(10)
    .then(function (topScores) {
      res.render("index", { topScores: topScores });
    });
});
/*
router.post("/", function (req, res) {
    var newFinalScore = new Scores({
        name: req.body.name,
        score: req.body.score,
        date: req.body.date
    });
    newFinalScore.save().then(function (Scores) {
        res.json(Scores);
    })
});*/
module.exports = router;
