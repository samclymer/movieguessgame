// GLOBAL VARIABLES (accessible by all functions)
var chosenMovie = "";
var chosenDate = "";
var chosenGenre = "";
// This will break the solution into individual letters to be stored in array.
var lettersInChosenMovieTitle = [];
// This will be the number of blanks we show based on the solution
var numBlanks = 0;
// Holds a mix of blank and solved letters (ex: 'n, _ _, n, _').
var blanksAndSuccesses = [];
var movieID = "";
var ombdAPIkey = "";
var ombdQueryURL = "";
var currentGuesses = [];
// Game counters
var name = "";
var score = 0;
var missesLeft = 5;
var timeLeft = 120;
var timer;
// Variable for dynamically applying game start/stop button
var button = $("#main-button");
button.html('<button id ="new-game" type="button" class="shadow startButton btn btn-primary" data-toggle="modal" data-target="#gameStartModal">NEW GAME</button>');
// variables for updating the modal shown at the end of the game
var recapStats = $("#recap-stats");
var recapScores = $("#recap-scores");
var recapWord = $("#recap-word");
var loadingMessage = $("#loading");
//Grabbing the element resposible for submitting letter choices, and disabling it until the game is actually in progress
document.getElementById("enter-button").disabled = true;

// FUNCTIONS (These are bits of code that we will call upon to run when needed)
// ========================================================================================

//The searchMovieStart and searchMovie Continue functions reach out through the imdb API to retrieve a movie title, specific to the rules based in the "if statement" upon an AJAX success. 
function searchMovieStart() {
  document.getElementById("new-game").disabled = true;
  document.getElementById("word-blanks").innerHTML = "Loading Movie - Get Ready!";
  movieID = Math.floor((Math.random() * 999999) + 100000);
  ombdAPIkey = "&apikey=e7fda2c7";
  ombdQueryURL = "https://www.omdbapi.com/?i=tt0" + movieID + ombdAPIkey;

  $.ajax({
    url: ombdQueryURL,
    method: "GET",
    dataType: "json",
    success: function (response) {
      if (response.Title && response.Year >= "1950" && response.Year <= "2019" && response.Genre !== "Adult" && response.Genre !== "N/A" && response.Released !== "N/A" && response.Title.length >= 1 && response.Title.length <= 25 && response.Country == "USA" && response.Language === "English" && response.Type === "movie") {
        document.getElementById("word-blanks").innerHTML = blanksAndSuccesses;
        document.getElementById("new-game").disabled = false;
        document.getElementById("enter-button").disabled = false;
        startGame(response);
      } else {
        searchMovieStart();
      }
    },
    error: function () {
      searchMovieStart();
    }
  })
};

function searchMovieContinue() {
  document.getElementById("end-game").disabled = true;
  document.getElementById("word-blanks").innerHTML = "Loading Movie - Get Ready!";
  movieID = Math.floor((Math.random() * 999999) + 100000);
  ombdAPIkey = "&apikey=e7fda2c7";
  ombdQueryURL = "https://www.omdbapi.com/?i=tt0" + movieID + ombdAPIkey;

  $.ajax({
    url: ombdQueryURL,
    method: "GET",
    dataType: "json",
    success: function (response) {
      if (response.Title && response.Year >= "1950" && response.Year <= "2019" && response.Genre !== "Adult" && response.Genre !== "N/A" && response.Released !== "N/A" && response.Title.length >= 1 && response.Title.length <= 25 && response.Country == "USA" && response.Language === "English" && response.Type === "movie") {
        document.getElementById("word-blanks").innerHTML = blanksAndSuccesses;
        document.getElementById("end-game").disabled = false;
        document.getElementById("enter-button").disabled = false;
        continueGame(response);
      } else {
        searchMovieContinue();
      }
    },
    error: function () {
      searchMovieContinue();
    }
  })
};

//Function is used to call on the MongoDB for the latest in player scores, with dates and names included
function getScores() {
  $('#highScoresModal').modal("show");
}

//Function is used to open the modal with the game's instructions
function getInstructions() {
  $('#instructionsModal').modal("show");
}

//Function that takes the player name, score and date, and records it to the MongoDB
function saveScore() {
  var newName = $("#name-input").val();
  console.log(newName);
  var newScore = score;
  console.log(newScore);
  var d = new Date();
  var newDate = d.toLocaleDateString();
  var finalResults = { name: newName, score: newScore, date: newDate };
  console.log(finalResults);
  $.post("/api/scoreRoute", finalResults).then(function () {
    reset();
  })
}

//Countdown clock for gameplay
function countdown() {
  timeLeft--;
  $("#time-left").text(timeLeft);
  if (timeLeft < 1 && timeLeft > -1) {
    recap();
  }
}

//In the event that the game has concluded, this function takes the player name/score, as well as the final movie title, and places them in a modal for display.  Upon closing the modal, the saveScore() funciton is run
function recap() {
  clearInterval(timer);
  recapStats.html('<h5 class="smallText2">Final Score: ' + score + '</h5>');
  recapWord.html('<h5 class="smallText2">Previous Movie Title: ' + chosenMovie + '</h5>');
  $('#gameEndModal').modal("show");
  $('#gameEndModal').one("hidden.bs.modal", function () {
    saveScore();
  });
}

//Modal for updates between rounds
function continueRecap() {
  clearInterval(timer);
  recapStats.html('<h5 class="smallText2">Current Score: ' + score + '</h5>');
  console.log(score);
  recapWord.html('<h5 class="smallText2">Previous Movie Title: ' + chosenMovie + '</h5>');
  $('#continueModal').modal("show");
  $('#continueModal').one("hidden.bs.modal", function () {
    continueReset();
  });
}

//Function used to reset the game completely
function reset() {
  clearInterval(timer);
  name = "";
  score = 0;
  missesLeft = 5;
  timeLeft = 120;
  chosenDate = "";
  chosenGenre = "";
  blanksAndSuccesses = [];
  document.getElementById("enter-button").disabled = true;
  button.html('<button id ="new-game" type="button" class="shadow startButton btn btn-primary" data-toggle="modal" data-target="#gameStartModal">NEW GAME</button>');
  document.getElementById("misses-left").innerHTML = missesLeft;
  document.getElementById("score").innerHTML = score;
  document.getElementById("time-left").innerHTML = timeLeft;
  document.getElementById("name-input").value = "";
  document.getElementById("name").innerHTML = name;
  document.getElementById("a").className = "col letter border rounded border-dark";
  document.getElementById("b").className = "col letter border rounded border-dark";
  document.getElementById("c").className = "col letter border rounded border-dark";
  document.getElementById("d").className = "col letter border rounded border-dark";
  document.getElementById("e").className = "col letter border rounded border-dark";
  document.getElementById("f").className = "col letter border rounded border-dark";
  document.getElementById("g").className = "col letter border rounded border-dark";
  document.getElementById("h").className = "col letter border rounded border-dark";
  document.getElementById("i").className = "col letter border rounded border-dark";
  document.getElementById("j").className = "col letter border rounded border-dark";
  document.getElementById("k").className = "col letter border rounded border-dark";
  document.getElementById("l").className = "col letter border rounded border-dark";
  document.getElementById("m").className = "col letter border rounded border-dark";
  document.getElementById("n").className = "col letter border rounded border-dark";
  document.getElementById("o").className = "col letter border rounded border-dark";
  document.getElementById("p").className = "col letter border rounded border-dark";
  document.getElementById("q").className = "col letter border rounded border-dark";
  document.getElementById("r").className = "col letter border rounded border-dark";
  document.getElementById("s").className = "col letter border rounded border-dark";
  document.getElementById("t").className = "col letter border rounded border-dark";
  document.getElementById("u").className = "col letter border rounded border-dark";
  document.getElementById("v").className = "col letter border rounded border-dark";
  document.getElementById("w").className = "col letter border rounded border-dark";
  document.getElementById("x").className = "col letter border rounded border-dark";
  document.getElementById("y").className = "col letter border rounded border-dark";
  document.getElementById("z").className = "col letter border rounded border-dark";
  document.getElementById("0").className = "col letter border rounded border-dark";
  document.getElementById("1").className = "col letter border rounded border-dark";
  document.getElementById("2").className = "col letter border rounded border-dark";
  document.getElementById("3").className = "col letter border rounded border-dark";
  document.getElementById("4").className = "col letter border rounded border-dark";
  document.getElementById("5").className = "col letter border rounded border-dark";
  document.getElementById("6").className = "col letter border rounded border-dark";
  document.getElementById("7").className = "col letter border rounded border-dark";
  document.getElementById("8").className = "col letter border rounded border-dark";
  document.getElementById("9").className = "col letter border rounded border-dark";
  document.getElementById("word-blanks").innerHTML = blanksAndSuccesses;
  document.getElementById("date-released").innerHTML = chosenDate;
  document.getElementById("genre").innerHTML = chosenGenre;
}

//Function used to reset the game for a new round, while maintaining the current player's name and score
function continueReset() {
  clearInterval(timer);
  movieID = 0;
  ombdAPIkey = "";
  ombdQueryURL = "";
  missesLeft = 5;
  timeLeft = 120;
  chosenDate = "";
  chosenGenre = "";
  chosenMovie = "";
  blanksAndSuccesses = [];
  document.getElementById("enter-button").disabled = true;
  button.html('<button onclick="recap()" id ="end-game" type="button" class="shadow startButton btn btn-danger" data-toggle="modal" data-target="#gameEndModal">END GAME</button>');
  document.getElementById("misses-left").innerHTML = missesLeft;
  document.getElementById("score").innerHTML = score;
  document.getElementById("time-left").innerHTML = timeLeft;
  document.getElementById("a").className = "col letter border rounded border-dark";
  document.getElementById("b").className = "col letter border rounded border-dark";
  document.getElementById("c").className = "col letter border rounded border-dark";
  document.getElementById("d").className = "col letter border rounded border-dark";
  document.getElementById("e").className = "col letter border rounded border-dark";
  document.getElementById("f").className = "col letter border rounded border-dark";
  document.getElementById("g").className = "col letter border rounded border-dark";
  document.getElementById("h").className = "col letter border rounded border-dark";
  document.getElementById("i").className = "col letter border rounded border-dark";
  document.getElementById("j").className = "col letter border rounded border-dark";
  document.getElementById("k").className = "col letter border rounded border-dark";
  document.getElementById("l").className = "col letter border rounded border-dark";
  document.getElementById("m").className = "col letter border rounded border-dark";
  document.getElementById("n").className = "col letter border rounded border-dark";
  document.getElementById("o").className = "col letter border rounded border-dark";
  document.getElementById("p").className = "col letter border rounded border-dark";
  document.getElementById("q").className = "col letter border rounded border-dark";
  document.getElementById("r").className = "col letter border rounded border-dark";
  document.getElementById("s").className = "col letter border rounded border-dark";
  document.getElementById("t").className = "col letter border rounded border-dark";
  document.getElementById("u").className = "col letter border rounded border-dark";
  document.getElementById("v").className = "col letter border rounded border-dark";
  document.getElementById("w").className = "col letter border rounded border-dark";
  document.getElementById("x").className = "col letter border rounded border-dark";
  document.getElementById("y").className = "col letter border rounded border-dark";
  document.getElementById("z").className = "col letter border rounded border-dark";
  document.getElementById("0").className = "col letter border rounded border-dark";
  document.getElementById("1").className = "col letter border rounded border-dark";
  document.getElementById("2").className = "col letter border rounded border-dark";
  document.getElementById("3").className = "col letter border rounded border-dark";
  document.getElementById("4").className = "col letter border rounded border-dark";
  document.getElementById("5").className = "col letter border rounded border-dark";
  document.getElementById("6").className = "col letter border rounded border-dark";
  document.getElementById("7").className = "col letter border rounded border-dark";
  document.getElementById("8").className = "col letter border rounded border-dark";
  document.getElementById("9").className = "col letter border rounded border-dark";
  document.getElementById("word-blanks").innerHTML = blanksAndSuccesses;
  document.getElementById("date-released").innerHTML = chosenDate;
  document.getElementById("genre").innerHTML = chosenGenre;
  searchMovieContinue();
}

//Function to start the game, resetting the score, misses left, and the timer.
function startGame(data) {
  missesLeft = 5;
  score = 0;
  timer = setInterval(countdown, 1000);
  //Movie title, genre and release date are created as variables for placement on screen
  chosenMovie = data.Title.toLowerCase();
  chosenGenre = data.Genre;
  chosenDate = data.Released;
  // The movie title is broken into individual letters.
  lettersInChosenMovieTitle = chosenMovie.split("");
  // We count the number of letters in the title.
  numBlanks = lettersInChosenMovieTitle.length;
  // We print the solution in console (for testing).
  console.log(chosenMovie);

  // CRITICAL LINE - Here we *reset* the guess and success array at each round.
  blanksAndSuccesses = [];
  currentGuesses = [];
  // Fill up the blanksAndSuccesses list with appropriate number of blanks.
  // This is based on number of letters in solution.
  for (var i = 0; i < numBlanks; i++) {
    var alphaRegex = /[a-z]/i;
    var numericalRegex = /[0-9]/;
    var spaceRegex = /\s/;
    if (lettersInChosenMovieTitle[i].match(alphaRegex) || lettersInChosenMovieTitle[i].match(numericalRegex)) {
      blanksAndSuccesses.push("_");
    } else if (lettersInChosenMovieTitle[i].match(spaceRegex)) {
      blanksAndSuccesses.push('\xa0');
    } else {
      blanksAndSuccesses.push(lettersInChosenMovieTitle[i]);
    }
  }

  var playerName = document.getElementById("name-input").value;

  document.getElementById("letter-input").value = "";
  document.getElementById("misses-left").innerHTML = missesLeft;
  document.getElementById("score").innerHTML = score;
  document.getElementById("time-left").innerHTML = timeLeft;
  document.getElementById("name").innerHTML = playerName;
  button.html('<button onclick="recap()" id ="end-game" type="button" class="shadow startButton btn btn-danger" data-toggle="modal" data-target="#gameEndModal">END GAME</button>');
  recapStats.html('<h5 class="smallText2">Final Score: ' + score + '</h5>');
  recapWord.html('<h5 class="smallText2">Previous Movie Title: ' + chosenMovie + '</h5>');
  // Prints the blanks at the beginning of each round in the HTML
  document.getElementById("word-blanks").innerHTML = blanksAndSuccesses.join(" ");
  document.getElementById("date-released").innerHTML = chosenDate;
  document.getElementById("genre").innerHTML = chosenGenre;
  document.getElementById("a").className = "col letter border rounded border-dark";
  document.getElementById("b").className = "col letter border rounded border-dark";
  document.getElementById("c").className = "col letter border rounded border-dark";
  document.getElementById("d").className = "col letter border rounded border-dark";
  document.getElementById("e").className = "col letter border rounded border-dark";
  document.getElementById("f").className = "col letter border rounded border-dark";
  document.getElementById("g").className = "col letter border rounded border-dark";
  document.getElementById("h").className = "col letter border rounded border-dark";
  document.getElementById("i").className = "col letter border rounded border-dark";
  document.getElementById("j").className = "col letter border rounded border-dark";
  document.getElementById("k").className = "col letter border rounded border-dark";
  document.getElementById("l").className = "col letter border rounded border-dark";
  document.getElementById("m").className = "col letter border rounded border-dark";
  document.getElementById("n").className = "col letter border rounded border-dark";
  document.getElementById("o").className = "col letter border rounded border-dark";
  document.getElementById("p").className = "col letter border rounded border-dark";
  document.getElementById("q").className = "col letter border rounded border-dark";
  document.getElementById("r").className = "col letter border rounded border-dark";
  document.getElementById("s").className = "col letter border rounded border-dark";
  document.getElementById("t").className = "col letter border rounded border-dark";
  document.getElementById("u").className = "col letter border rounded border-dark";
  document.getElementById("v").className = "col letter border rounded border-dark";
  document.getElementById("w").className = "col letter border rounded border-dark";
  document.getElementById("x").className = "col letter border rounded border-dark";
  document.getElementById("y").className = "col letter border rounded border-dark";
  document.getElementById("z").className = "col letter border rounded border-dark";
  document.getElementById("0").className = "col letter border rounded border-dark";
  document.getElementById("1").className = "col letter border rounded border-dark";
  document.getElementById("2").className = "col letter border rounded border-dark";
  document.getElementById("3").className = "col letter border rounded border-dark";
  document.getElementById("4").className = "col letter border rounded border-dark";
  document.getElementById("5").className = "col letter border rounded border-dark";
  document.getElementById("6").className = "col letter border rounded border-dark";
  document.getElementById("7").className = "col letter border rounded border-dark";
  document.getElementById("8").className = "col letter border rounded border-dark";
  document.getElementById("9").className = "col letter border rounded border-dark";
}

function continueGame(data) {

  missesLeft = 5;
  timeLeft = 120;
  timer = setInterval(countdown, 1000);
  chosenMovie = data.Title.toLowerCase();
  chosenGenre = data.Genre;
  chosenDate = data.Released;
  // The word is broken into individual letters.
  lettersInChosenMovieTitle = chosenMovie.split("");
  // We count the number of letters in the word.
  numBlanks = lettersInChosenMovieTitle.length;
  // We print the solution in console (for testing).
  console.log(chosenMovie);

  // CRITICAL LINE - Here we *reset* the guess and success array at each round.
  blanksAndSuccesses = [];
  currentGuesses = [];
  // Fill up the blanksAndSuccesses list with appropriate number of blanks.
  // This is based on number of letters in solution.
  for (var i = 0; i < numBlanks; i++) {
    var alphaRegex = /[a-z]/i;
    var numericalRegex = /[0-9]/;
    var spaceRegex = /\s/;
    if (lettersInChosenMovieTitle[i].match(alphaRegex) || lettersInChosenMovieTitle[i].match(numericalRegex)) {
      blanksAndSuccesses.push("_");
    } else if (lettersInChosenMovieTitle[i].match(spaceRegex)) {
      blanksAndSuccesses.push('\xa0');
    } else {
      blanksAndSuccesses.push(lettersInChosenMovieTitle[i]);
    }
  }

  document.getElementById("misses-left").innerHTML = missesLeft;
  document.getElementById("score").innerHTML = score;
  document.getElementById("time-left").innerHTML = timeLeft;
  // Prints the blanks at the beginning of each round in the HTML
  document.getElementById("word-blanks").innerHTML = blanksAndSuccesses.join(" ");
  document.getElementById("date-released").innerHTML = chosenDate;
  document.getElementById("genre").innerHTML = chosenGenre;
  recapStats.html('<h5 class="smallText2">Final Score: ' + score + '</h5>');
  recapWord.html('<h5 class="smallText2">Previous Movie Title: ' + chosenMovie + '</h5>');
  document.getElementById("a").className = "col letter border rounded border-dark";
  document.getElementById("b").className = "col letter border rounded border-dark";
  document.getElementById("c").className = "col letter border rounded border-dark";
  document.getElementById("d").className = "col letter border rounded border-dark";
  document.getElementById("e").className = "col letter border rounded border-dark";
  document.getElementById("f").className = "col letter border rounded border-dark";
  document.getElementById("g").className = "col letter border rounded border-dark";
  document.getElementById("h").className = "col letter border rounded border-dark";
  document.getElementById("i").className = "col letter border rounded border-dark";
  document.getElementById("j").className = "col letter border rounded border-dark";
  document.getElementById("k").className = "col letter border rounded border-dark";
  document.getElementById("l").className = "col letter border rounded border-dark";
  document.getElementById("m").className = "col letter border rounded border-dark";
  document.getElementById("n").className = "col letter border rounded border-dark";
  document.getElementById("o").className = "col letter border rounded border-dark";
  document.getElementById("p").className = "col letter border rounded border-dark";
  document.getElementById("q").className = "col letter border rounded border-dark";
  document.getElementById("r").className = "col letter border rounded border-dark";
  document.getElementById("s").className = "col letter border rounded border-dark";
  document.getElementById("t").className = "col letter border rounded border-dark";
  document.getElementById("u").className = "col letter border rounded border-dark";
  document.getElementById("v").className = "col letter border rounded border-dark";
  document.getElementById("w").className = "col letter border rounded border-dark";
  document.getElementById("x").className = "col letter border rounded border-dark";
  document.getElementById("y").className = "col letter border rounded border-dark";
  document.getElementById("z").className = "col letter border rounded border-dark";
  document.getElementById("0").className = "col letter border rounded border-dark";
  document.getElementById("1").className = "col letter border rounded border-dark";
  document.getElementById("2").className = "col letter border rounded border-dark";
  document.getElementById("3").className = "col letter border rounded border-dark";
  document.getElementById("4").className = "col letter border rounded border-dark";
  document.getElementById("5").className = "col letter border rounded border-dark";
  document.getElementById("6").className = "col letter border rounded border-dark";
  document.getElementById("7").className = "col letter border rounded border-dark";
  document.getElementById("8").className = "col letter border rounded border-dark";
  document.getElementById("9").className = "col letter border rounded border-dark";
}

// checkLetters() function
// It's where we will do all of the comparisons for matches.
function checkLetters(letter) {

  // This boolean will be toggled based on whether or not a user letter is found anywhere in the word.
  var letterInWord = false;

  // Check if a letter exists inside the array at all.
  for (var i = 0; i < numBlanks; i++) {
    if (chosenMovie[i] === letter) {
      // If the letter exists then toggle this boolean to true. This will be used in the next step.
      letterInWord = true;
    }
  }

  // If the letter exists somewhere in the word, then figure out exactly where (which indices).
  if (letterInWord) {

    // Loop through the word.
    for (var j = 0; j < numBlanks; j++) {

      // Populate the blanksAndSuccesses with every instance of the letter.
      if (chosenMovie[j] === letter) {
        // Here we set the specific space in blanks and letter equal to the letter when there is a match.
        blanksAndSuccesses[j] = letter;
        document.getElementById(letter).className = "col bg-success letter border rounded border-dark";
        currentGuesses.push(letter);
        score++;
      }
    }
  }
  // If the letter doesn't exist at all...
  else {
    // ..then we add the letter to the list of wrong letters, and we subtract one of the guesses.
    document.getElementById(letter).className = "col bg-danger letter border rounded border-dark";
    currentGuesses.push(letter);
    missesLeft--;
  }
}

// roundComplete() function
// Here we will have all of the code that needs to be run after each guess is made
function roundComplete() {

  // Update the HTML to reflect the new number of guesses. Also update the correct guesses.
  document.getElementById("misses-left").innerHTML = missesLeft;
  // This will print the array of guesses and blanks onto the page.
  document.getElementById("word-blanks").innerHTML = blanksAndSuccesses.join(" ");
  document.getElementById("score").innerHTML = score;
  document.getElementById("letter-input").value = "";
  recapStats.html('<h5 class="smallText2">Final Score: ' + score + '</h5>');
  recapWord.html('<h5 class="smallText2">Previous Movie Title: ' + chosenMovie + '</h5>');
  // If we have gotten all the letters to match the solution...

  if (lettersInChosenMovieTitle.toString() == blanksAndSuccesses.toString().replace(/\xa0/g, " ")) {
    // ..add to the win counter & give the user an alert.
    continueRecap();
  }

  // If we've run out of guesses..
  else if (missesLeft === 0) {
    // Give the user an alert.
    recap();
  }
  else {

  }
}

// MAIN PROCESS (THIS IS THE CODE THAT CONTROLS WHAT IS ACTUALLY RUN)
// ==================================================================================================

// Get the input field
var input = document.getElementById("letter-input");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keypress", function (event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("enter-button").click();
  }
});

// Then initiate the function for capturing key clicks.
function processGame() {
  // Converts all key clicks to lowercase letters.
  var placeholder = document.getElementById("letter-input").value;
  var letterGuessed = placeholder.toLowerCase();
  var checkSubmit = currentGuesses.indexOf(letterGuessed);

  if (checkSubmit === -1) {
    // Runs the code to check for correctness.
    checkLetters(letterGuessed);
    // Runs the code after each round is done.
    roundComplete();
  } else {
    alert("Please pick an unused letter");
    document.getElementById("letter-input").value = "";
  }
};

