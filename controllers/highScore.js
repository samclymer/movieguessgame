var db = require("../models");

module.exports = {
  /* Find
  find: function (req, res) {
    db.Scores.find(req.query).sort({ score: -1 }).then(function (dbScore) {
      res.json(dbScore);
    });
  },
  */
  // Create a new Score
  create: function (req, res) {
    db.Scores.create(req.body).then(function (dbScore) {
      res.json(dbScore);
    });
  }/*,
  // Delete
  delete: function (req, res) {
    db.Scores.remove({ _id: req.params.id }).then(function (dbScore) {
      res.json(dbScore);
    });
  }*/
};

